# SatNOGS Data Warehouse / Dashboard

Docker-compose files for influxdb and grafana, to be used for SatNOGS Data Warehouse and Dashboard.

This is a derivative of <https://github.com/nicolargo/docker-influxdb-grafana> (minus telegraf)

Run your stack:

```
docker-compose up -d

```

Show me the logs:

```
docker-compose logs
```

Test the InfluxDB setup (show databases):

```
curl -G http://localhost:8086/query?pretty=true --data-urlencode "db=glances" --data-urlencode "q=SHOW DATABASES"
```

Create a new database:

```
curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=CREATE DATABASE mydb'
```

Access Grafana: <http://127.0.0.1:3000> - admin/admin

Stop it:

```
docker-compose stop
docker-compose rm
```

Update it:

```
git pull
docker pull grafana/grafana
docker pull influxdb
```

## License
AGPL-3.0-or-later
